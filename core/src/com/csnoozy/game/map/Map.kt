package com.csnoozy.game.map

import box2dLight.ConeLight
import box2dLight.PointLight
import box2dLight.RayHandler
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.maps.MapGroupLayer
import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.MapLayers
import com.badlogic.gdx.maps.objects.PolygonMapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapImageLayer
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Disposable
import com.csnoozy.game.camera.Camera2d
import com.csnoozy.game.filter.*
import com.csnoozy.game.lib.B2DSeparator
import ktx.inject.Context
import java.util.*

class Map(private val context: Context) : Disposable {

    private val assetManager: AssetManager = context.inject()
    private val batch: SpriteBatch = context.inject()
    private val camera: Camera2d = context.inject()
    private val world: World = context.inject()
    private val rayHandler: RayHandler = context.inject()

    private val maps: ArrayList<OrthogonalTiledMapRenderer> = ArrayList()
    private val worldCollision: ArrayList<Body> = ArrayList()

    init {
        loadMap(assetManager["maps/Canyon A.tmx"], Vector2.Zero)
    }

    fun renderUpper() {
        maps.forEach {
            val renderer = it
            it.map.layers.filter { it.properties["Render Stage"] == 2}.forEach{renderer.renderMapLayer(it)}
        }
    }

    fun renderLower() {
        maps.forEach {
            it.setView(camera.camera)
            val renderer = it
            it.map.layers.filter { it.properties["Render Stage"] == 1}.forEach{renderer.renderMapLayer(it)}
        }
    }

    private fun loadMap(map: TiledMap, offset: Vector2) {
        val mapRenderer = OrthogonalTiledMapRenderer(map, 1f / 32f, batch)
        maps.add(mapRenderer)

        map.layers["Static Light"].objects.forEach {
            it as PolygonMapObject
            it.polygon.apply {
                val color = Color(it.properties["Color"] as Color )
                when (it.properties["type"]) {
                    "cone" -> {
                        ConeLight(rayHandler, 75, color, 10f, this.x / 32, this.y / 32, -90f, 75f).apply {
                            isStaticLight = false
                            setContactFilter(LIGHT, LIGHT_GROUP, MASK_LIGHTS)
                        }
                    }
                    "point" -> {
                        PointLight(rayHandler, 75, color, 10f, this.x / 32, this.y / 32).apply {
                            isStaticLight = false
                            setContactFilter(LIGHT, LIGHT_GROUP, MASK_LIGHTS)
                        }
                    }
                }
            }
        }

        map.layers["Collision Layer"].objects.forEach {
            it as PolygonMapObject
            it.polygon.apply {
                val wallDef = BodyDef().apply {
                    position.set(offset)
                }
                val body = world.createBody(wallDef)

                val worldVertices = FloatArray(this.transformedVertices.size, {
                    this.transformedVertices[it] * 3f
                }).toVector2d()

                println("Collision Layer " + it.name + " validation: " + B2DSeparator().validate(worldVertices))

                B2DSeparator().separate(body,
                        FixtureDef().apply {
                            filter.categoryBits = if (it.properties["type"] == "wall") OBSTACLE else LIGHTPASS_OBSTACLE
                        },
                        if (B2DSeparator().validate(worldVertices) == 0) worldVertices else worldVertices.reversedArray()
                )

                worldCollision.add(body)
            }
        }
    }

    private fun OrthogonalTiledMapRenderer.renderMapLayer(layer: MapLayer) {
        if (!layer.isVisible) return
        if (layer is MapGroupLayer) {
            val childLayers = layer.layers
            for (i in 0..childLayers.size()) {
                val childLayer = childLayers.get(i)
                if (!childLayer.isVisible) continue
                renderMapLayer(childLayer)
            }
        } else {
            when (layer) {
                is TiledMapTileLayer -> renderTileLayer(layer)
                is TiledMapImageLayer -> renderImageLayer(layer)
                else -> renderObjects(layer)
            }
        }
    }

    override fun dispose() {
    }

    private fun FloatArray.toVector2d(): Array<out Vector2> = Array(this.size / 2, { Vector2(this[it * 2], this[it * 2 + 1]) })
}