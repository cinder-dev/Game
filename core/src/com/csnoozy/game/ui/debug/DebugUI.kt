package com.csnoozy.game.ui.debug

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.csnoozy.game.camera.Camera2d
import com.csnoozy.game.input.MyInputListener
import com.csnoozy.game.ui.skins.DebugSkin
import ktx.actors.onClick
import ktx.app.LetterboxingViewport
import ktx.inject.Context
import ktx.scene2d.Scene2DSkin
import ktx.scene2d.label
import ktx.scene2d.tree

private const val PPI: Float = 96f

class DebugUI(context: Context) : Stage(LetterboxingViewport(PPI, PPI, Gdx.graphics.width.toFloat() / Gdx.graphics.height.toFloat())) {

    private val assetManager: AssetManager = context.inject()
    private val input: MyInputListener = context.inject()
    private val camera: Camera2d = context.inject()

    private val newLabels = ArrayList<BoundLabels>()

    init {
        isDebugAll = true
        Scene2DSkin.defaultSkin = DebugSkin(context).debugSkin
        this.addActor(tree {
            setFillParent(true)
            label("System") { node ->
                node {
                    add(BoundLabels { "FPS: ${Gdx.graphics.framesPerSecond}" }.apply { newLabels.add(this) })
                    add(BoundLabels { "Memory: ${Gdx.app.javaHeap} / ${Gdx.app.nativeHeap}" }.apply { newLabels.add(this) })
                    label("Asset Manager Diagnostics") { node ->
                        node {
                            add(BoundLabels { assetManager.diagnostics }.apply { newLabels.add(this) })
                        }
                    }.apply { onClick { inNode.isExpanded = !inNode.isExpanded } }
                }
            }.apply { onClick { inNode.isExpanded = !inNode.isExpanded } }
            label("Camera") { node ->
                node {
                    add(BoundLabels { "Position: (${camera.camera.position.x}, ${camera.camera.position.y}, ${camera.camera.position.z})" }.apply { newLabels.add(this) })
                    add(BoundLabels { "Zoom: ${camera.scale}" }.apply { newLabels.add(this) })
                }
            }.apply { onClick { inNode.isExpanded = !inNode.isExpanded } }
            label("Mouse") { node ->
                node {
                    add(BoundLabels { "Position: (${input.mouse.x}, ${input.mouse.y})" }.apply { newLabels.add(this) })
                    add(BoundLabels { "Local: (${input.mouseInWorld.x}, ${input.mouseInWorld.y})" }.apply { newLabels.add(this) })
                }
            }.apply { onClick { inNode.isExpanded = !inNode.isExpanded } }
        })
    }

    fun render(delta: Float) {
        act(delta)
        newLabels.forEach(BoundLabels::update)

        draw()
    }

    fun resize(width: Int, height: Int){
        viewport.update(width, height, true)
    }
}

private class BoundLabels(private val text: () -> String) : Label(text(), Scene2DSkin.defaultSkin) {

    fun update() {
        setText(text())
    }
}