package com.csnoozy.game.filter

import kotlin.experimental.or

// Categories
const val OBSTACLE: Short = 1
const val PLAYER: Short = 1 shl 1
const val LIGHTPASS_OBSTACLE: Short = 1 shl 2
const val PICKUP: Short = 1 shl 3
const val LIGHT: Short = 1 shl 4
const val SENSOR: Short = 1 shl 5
const val BODY_PART: Short = 1 shl 6


const val LIGHT_GROUP: Short = 1

// Masks
val MASK_LIGHTS = OBSTACLE or PLAYER
val MASK_LIGHTS_NO_WALLS = PLAYER
val MASK_PLAYER = OBSTACLE or LIGHTPASS_OBSTACLE or PICKUP or SENSOR
val MASK_PICKUP = PICKUP or PLAYER or OBSTACLE or LIGHTPASS_OBSTACLE
val MASK_SENSOR = PLAYER
val MASK_BODY_PART = OBSTACLE or LIGHTPASS_OBSTACLE