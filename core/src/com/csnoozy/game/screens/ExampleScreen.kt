package com.csnoozy.game.screens

import box2dLight.PointLight
import box2dLight.RayHandler
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.csnoozy.game.camera.Camera2d
import com.csnoozy.game.filter.LIGHT
import com.csnoozy.game.filter.LIGHT_GROUP
import com.csnoozy.game.filter.MASK_LIGHTS
import com.csnoozy.game.input.MyInputListener
import com.csnoozy.game.map.Map
import com.csnoozy.game.player.Player
import com.csnoozy.game.ui.debug.DebugUI
import ktx.app.KtxScreen
import ktx.app.clearScreen
import ktx.app.use
import ktx.assets.load
import ktx.inject.Context

class ExampleScreen(private val context: Context) : KtxScreen {

    init {
        context.register {
            bindSingleton {
                World(Vector2(0f, 0f), true)
            }
            bindSingleton {
                SpriteBatch()
            }
            bindSingleton {
                Player(this)
            }
            bindSingleton {
                Camera2d(this)
            }
            bindSingleton {
                MyInputListener(this)
            }
            bindSingleton {
                RayHandler(this.inject()).apply {
                    setShadows(true)
                    setBlur(true)
                    setCulling(true)
                }
            }
        }
    }

    private val assetManager: AssetManager = context.inject<AssetManager>().apply {
        load<Texture>("grid.png")
        load<Texture>("tree.png")
        load<Texture>("set.png")
        setLoader(TiledMap().javaClass, TmxMapLoader(InternalFileHandleResolver()))
        load<TiledMap>("maps/test.tmx")
        load<TiledMap>("maps/Canyon A.tmx")
        finishLoading()
    }
    private val camera: Camera2d = context.inject()
    private val world: World = context.inject()
    private val batch: SpriteBatch = context.inject()
    private val input: MyInputListener = context.inject()
    private val player: Player = context.inject()
    private val rayHandler: RayHandler = context.inject()

    private val debugUI = DebugUI(context)
    private val debugRenderer: Box2DDebugRenderer = Box2DDebugRenderer()
    private val map = Map(context)

    private val playerLight = PointLight(rayHandler, 365, Color(0.5f, 0.4f, 0.3f, 0.7f), 25f, player.body.position.x, player.body.position.y).apply {
        this.attachToBody(player.body)
        setContactFilter(LIGHT, LIGHT_GROUP, MASK_LIGHTS)
    }

    init {
        val multiplexer = InputMultiplexer()
        multiplexer.addProcessor(input)
        multiplexer.addProcessor(debugUI)
        Gdx.input.inputProcessor = multiplexer
    }

    private val timeStep: Float = 1f / 45f
    private var accumulator: Float = 0f
    private fun doPhysicsStep(delta: Float) {
        val frameTime = Math.min(delta, 0.25f)
        accumulator += frameTime
        while (accumulator >= timeStep) {
            world.step(timeStep, 6, 2)
            rayHandler.update()
            accumulator -= timeStep
        }
    }

    override fun render(delta: Float) {
        clearScreen(50f / 255f, 33f / 255f, 37f / 255f)
        camera.update()
        rayHandler.setCombinedMatrix(camera.camera)

        batch.use {
            map.renderLower()
            player.render(delta)
            map.renderUpper()
        }
        rayHandler.render()

//        debugRenderer.render(world, camera.camera.combined)

        doPhysicsStep(delta)
        debugUI.render(delta)
    }

    override fun dispose() {
        debugUI.dispose()
        rayHandler.dispose()
    }

    override fun resize(width: Int, height: Int) {
        debugUI.resize(width, height)
        camera.resize(width, height)
        camera.update()
    }
}
