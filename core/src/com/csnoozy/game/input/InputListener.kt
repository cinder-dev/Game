package com.csnoozy.game.input

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.csnoozy.game.camera.Camera2d
import com.csnoozy.game.player.Player
import ktx.app.KtxInputAdapter
import ktx.inject.Context

class MyInputListener(private val context: Context) : KtxInputAdapter {

    private val camera: Camera2d = context.inject()
    private val player: Player = context.inject()

    val mouse: Vector2 = Vector2(0f, 0f)
    val mouseInWorld: Vector3 = Vector3(0f, 0f, 0f)

    override fun keyDown(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.W -> player.up = true
            Input.Keys.S -> player.down = true
            Input.Keys.A -> player.left = true
            Input.Keys.D -> player.right = true
            Input.Keys.ESCAPE -> Gdx.app.exit()
        }
        return false
    }

    override fun keyUp(keycode: Int): Boolean {
        when (keycode) {
            Input.Keys.W -> player.up = false
            Input.Keys.S -> player.down = false
            Input.Keys.A -> player.left = false
            Input.Keys.D -> player.right = false
        }
        return false
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {

        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        mouse.set(screenX.toFloat(), screenY.toFloat())
        mouseInWorld.set(screenX.toFloat(), screenY.toFloat(), 0f)
        camera.camera.unproject(mouseInWorld)
        return false
    }

    override fun scrolled(amount: Int): Boolean {
        if (amount > 0)
            camera.zoom(0.1f)
        else
            camera.zoom(-0.1f)

        return false
    }
}